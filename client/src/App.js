import { observer } from "mobx-react-lite";
import { BrowserRouter } from "react-router-dom";
import { AppRoutes } from "./routes/AppRoutes";
import { Footer } from "./Components/Footer";
import { Header } from "./Components/Header";
import { useEffect } from "react";
import { useStore } from "../src/Hooks/useStore";
import "../src/CSS/app.css";

export const App = observer(() => {
  const { rootStore: { authStore } } = useStore();
  
  useEffect(() => {
    const token = localStorage.getItem('accessToken')
    if (token) {
      authStore.setLoginToken(token)
      authStore.fetchUser(token)
    }
  })
  return (
    <BrowserRouter>
      <div className="App">
        <Header />
        <AppRoutes />
        <Footer></Footer>
      </div>
    </BrowserRouter>
  );
});

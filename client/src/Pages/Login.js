import { observer } from "mobx-react-lite";
import { React, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useStore } from "../Hooks/useStore";

export const Login = observer(() => {
  const {
    rootStore: { authStore },
  } = useStore();

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error,setError] = useState("");

  const navigate = useNavigate();
  const onLogin = async () => {
    if (email !== "" && password !== "") {
      authStore.login(email, password)
        .catch((error) => {
          if (error.response) {
            setError("Wrong email or password")
          }
      })
    }
  };

  useEffect(() => {
    if (authStore.getUserDetails) {
      navigate("/products")
    }
  }, [authStore.getUserDetails, navigate])

  const onChangeEmail = (event) => {
    setEmail(event.target.value);
  };
  const onChangePassword = (event) => {
    setPassword(event.target.value);
  };

  const submitForm = (e)=>{
    e.preventDefault();
    setError("");
    if(email === ""){
      setError("Email is mandatory field!");
    }else if(password === ""){
      setError("Password is mandatory field!");
    }
  }

  return (
    <main className="form-signin mt-5">
      <h1 className="h3 mb-3 fw-normal mt-5">Please sign in</h1>
      <small className="text-danger">{error}</small>
      <form onSubmit={submitForm}>
        <div className="form-floating">
          <input
            type="email"
            id="floatingInput"
            className="form-control"
            value={email}
            onChange={onChangeEmail}
          />
          <label htmlFor="floatingInput">Email</label>
        </div>
        <div className="form-floating">
          <input
            type="password"
            id="floatingPassword"
            className="form-control"
            value={password}
            onChange={onChangePassword}
          />
          <label htmlFor="floatingPassword">Password</label>
        </div>
        <button
          onClick={onLogin}
          className="w-100 btn btn-lg btn-primary"
          type="submit"
        >
          Sign in
        </button>
        <p className="mt-5 mb-3 text-muted">&copy; 2022</p>
      </form>
    </main>
  );
});

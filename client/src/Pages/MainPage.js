import { Products } from "../Components/Products/Products";
import { TopSection } from "../Components/TopSection";

export const MainPage = () => {
  
  return (
    <main>
      <TopSection />
      <Products />
    </main>
  );
};

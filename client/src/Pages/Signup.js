import { observer } from "mobx-react-lite";
import { React, useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useStore } from "../Hooks/useStore";

export const Signup = observer(() => {
  const {
    rootStore: { authStore },
  } = useStore();

  const navigate = useNavigate();
  const [firsName, setFirstName] =useState("")
  const [LastName, setLastName] =useState("")
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [error,setError] = useState("");

  const onSignup = async () => {
    if (email !== "" && password !== "" && firsName !== "" && LastName !== "") {
      authStore.signup(firsName, LastName, email, password)
        .catch((error) => {
          if (error.response) {
            setError("Register Fail, please try again")
          }
      })
    }
  };

  useEffect(() => {
    if (authStore.getUserDetails) {
      navigate("/products")
    }
  }, [authStore.getUserDetails, navigate])

  const onChangeFirstName = (event) => {
    setFirstName(event.target.value);
  };
  const onChangeLastName = (event) => {
    setLastName(event.target.value);
  };
  const onChangeEmail = (event) => {
    setEmail(event.target.value);
  };
  const onChangePassword = (event) => {
    setPassword(event.target.value);
  };

  const submitForm = (e)=>{
    e.preventDefault();
    setError("");
    if(email === ""){
      setError("Email is mandatory field!");
    }else if(password === ""){
      setError("Password is mandatory field!");
    }else if(firsName === ""){
      setError("First name is mandatory field!");
    }else if(LastName === ""){
      setError("Last Name is mandatory field!");
    }
  }

  return (
    <main className="form-signin mt-5">
      <h1 className="h3 mb-3 fw-normal mt-5">Please sign up</h1>
      <small className="text-danger">{error}</small>
      <form onSubmit={submitForm}>
        <div className="form-floating">
          <input
            type="text"
            id="firstName"
            className="form-control"
            onChange={onChangeFirstName}
          />
          <label htmlFor="floatingInput">First Name</label>
        </div>
        <div className="form-floating">
          <input
            type="text"
            id="lastName"
            className="form-control"
            onChange={onChangeLastName}
          />
          <label htmlFor="floatingInput">Last Name</label>
        </div>
        <div className="form-floating">
          <input
            type="email"
            id="email"
            className="form-control"
            onChange={onChangeEmail}
          />
          <label htmlFor="floatingInput">Email</label>
        </div>
        <div className="form-floating">
          <input
            type="password"
            id="password"
            className="form-control"
            onChange={onChangePassword}
          />
          <label htmlFor="floatingPassword">Password</label>
        </div>
        <button
          onClick={onSignup}
          className="w-100 btn btn-lg btn-primary"
          type="submit"
        >
          Sign-up
        </button>
        <p className="mt-5 mb-3 text-muted">&copy; 2022</p>
      </form>
    </main>
  );
});


export const sortParams = [
    {
      title: "Sort By Name A-Z",
      sortBy: "name",
      order: "asc",
    },
    {
      title: "Sort By Name Z-A",
      sortBy: "name",
      order: "desc",
    },
    {
      title: "Sort By Price Low-High",
      sortBy: "price",
      order: "asc",
    },{
      title: "Sort By Price High-Low",
      sortBy: "price",
      order: "desc",
    }
  ]
import { action, computed, makeObservable, observable, runInAction } from "mobx";
import axios from "axios";
import { sortParams } from "./dataParam"

export class ProductStore {
  products = [];
  productParams = { sortBy: "created_at", order: "desc" };
  pageNumber = 1;
  sortParam = sortParams;
  currentProduct = {};
  isLoading= true;
  hasMore = true;

  constructor(rootStore) {
    makeObservable(this, {
      products: observable,
      currentProduct: observable,
      setIsLoading: action,
      setProducts: action,
      setProductParams: action,
      setPageNumber: action,
      fetchProducts: action,
      addProduct: action,
      deleteProduct: action,
      getProducts: computed,
      getCurrentProduct: computed,
    });
    this.rootStore = rootStore;
  }

  async fetchProducts() {
    const productsResponse = await axios.get(
      "http://localhost:5000/api/v1/products",
      {
        params: {limit:12, page: this.pageNumber, sort_by: this.productParams.sortBy, order: this.productParams.order},
        crossdomain: true,
      }
    );
    runInAction(() => {
      this.products = [...new Set([...this.products, ...productsResponse?.data?.data?.row ?? []])]
      if (productsResponse?.data?.data?.row.length > 0) {
        this.hasMore = true
      } else {
        this.hasMore = false
      }
    })
  }

  async addProduct(data) {
    const config = {
      headers: {
        'Authorization': `Bearer ${this.rootStore.authStore.getUserToken}`,
      }
    }
    await axios.post(`http://localhost:5000/api/v1/product`, data, config);
    this.setPageNumber(1)
    this.setProducts([])
    await this.fetchProducts()
  }

  async updateProduct(data) {
    const config = {
      headers: {
        'Authorization': `Bearer ${this.rootStore.authStore.getUserToken}`,
      }
    }
    const updateRes = await axios.put(`http://localhost:5000/api/v1/product/${this.currentProduct.id}`, data, config);
    return updateRes.data.product
  }

  async deleteProduct(product) {
    const config = {
      headers: {
        'Authorization': `Bearer ${this.rootStore.authStore.getUserToken}`,
      }
    }
    await axios.delete(`http://localhost:5000/api/v1/product/${product.id}`, config);
    const newProduct = this.products.filter(item => item.id !== product.id)
    this.setProducts(newProduct)
  }

  get getProducts() {
    return this.products;
  }

  get getCurrentProduct() {
    return this.currentProduct
  }

  get isMoreProducts() {
    return this.hasMore
  }

  setIsLoading(bool) {
    return this.isLoading = bool
  }

  setProducts(data) {
    return this.products = data
  }

  setProductParams(obj) {
    this.productParams = obj
  }

  setPageNumber(num) {
    this.pageNumber = num
  }

  setProductName(name) {
    this.currentProduct.name = name
  }

  setProductImage(url) {
    this.currentProduct.url = url
  }

  setProductPrice(price) {
    this.currentProduct.price = price
  }

  setProductDescription(description) {
    this.currentProduct.description = description
  }
}

import { AuthStore } from "./authStore";
import { ProductStore } from "./product/productStore";
import { UserStore } from "./userStore";

export class RootStore {
  constructor() {
    this.authStore = new AuthStore(this);
    this.productStore = new ProductStore(this);
    this.userStore = new UserStore(this);
  }
}

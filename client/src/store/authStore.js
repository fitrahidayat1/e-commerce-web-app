import { action, computed, makeObservable, observable } from "mobx";
import axios from "axios";

export class AuthStore {
  loginToken = "";
  statusCode = ""
  userDetails = "";
  constructor(rootStore) {
    makeObservable(this, {
      loginToken: observable,
      userDetails: observable,
      signup: action,
      login: action,
      logout: action,
      fetchUser: action,
      setLoginToken: action,
      setUserDetails: action,
      getUserToken: computed,
      getUserDetails: computed,
    });
    this.rootStore = rootStore;
  }

  async fetchUser(token) {
    const headers = {
      'Authorization': `Bearer ${token}`,
    }
    const userRes = await axios.get("http://localhost:5000/api/v1/user",
      {
        headers,
      });
    const dataUser = userRes.data.user
    this.setUserDetails(dataUser)
    // this.userDetails = userRes.data.user;
  }

  async login(email, password) {
    const data = {
      email,
      password,
    };

    const res = await axios.post(
      "http://localhost:5000/api/v1/auth/login",
      data
    );
    if (res.data.token) {
      this.loginToken = res.data.token;
      this.fetchUser(this.loginToken)
      localStorage.setItem("accessToken", this.loginToken)
    }
  }

  async logout() {
    this.loginToken = "";
    this.userDetails = null;
    localStorage.removeItem("accessToken")
  }

  async signup(first_name, last_name, email, password) {
    const data = {
      first_name,
      last_name,
      email,
      password
    }
    await axios.post(
      "http://localhost:5000/api/v1/auth/signup",
      data
    );
  }

  setLoginToken(token) {
    this.loginToken = token
  }

  setUserDetails(data) {
    this.userDetails = data
  }

  get getUserToken() {
    return this.loginToken;
  }

  get getIsAuthenticated() {
    return this.isAuthenticated;
  }

  get getUserDetails() {
    return this.userDetails;
  }
}
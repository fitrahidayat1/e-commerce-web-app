import { makeAutoObservable } from "mobx";

export class UserStore {
  name = "John doe";

  constructor() {
    makeAutoObservable(this);
  }

  setUserName = (name) => {
    this.name = name;
  };
}
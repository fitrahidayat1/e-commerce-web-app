import { Link, useNavigate } from "react-router-dom";
import { observer } from "mobx-react-lite";
import { useStore } from "../Hooks/useStore";

export const Header = observer(() => {
  const {
    rootStore: { authStore },
  } = useStore();

  const navigate = useNavigate();
  
  const onLogout = () => {
    authStore.logout();
    navigate("/");
  };

  return (
    <header className="p-3 bg-dark text-white fixed-top">
      <div className="container">
        <div className="d-flex flex-wrap align-items-center justify-content-center justify-content-lg-start">
          <Link
            to="/products"
            className="d-flex align-items-center mb-2 mb-lg-0 text-white text-decoration-none"
          >
            Ecommerce Web App
          </Link>
          <ul className="nav col-12 col-lg-auto me-lg-auto mb-2 justify-content-center mb-md-0"></ul>
          <div className="text-end">
            {!authStore.getUserDetails ?
              <>
                <Link to="/login" className="btn btn-primary me-2">
                  Login
                </Link>
                <Link to="/signup" className="btn btn-warning">
                  Sign-up
                </Link>
              </> :
              <>
                <span className="me-2">
                  {`Welcome ${authStore.userDetails?.firstName} ${authStore.userDetails?.lastName}`}
                </span>
                <button
                  type="button"
                  className="btn btn-warning"
                  onClick={onLogout}
                >
                  Logout
                </button>
              </>
            }
          </div>
        </div>
      </div>
    </header>
  );
});

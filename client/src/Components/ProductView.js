import { find } from "lodash";
import { observer } from "mobx-react-lite";
import { useParams } from "react-router-dom";
import { useStore } from "../Hooks/useStore";

export const ProductView = observer(() => {
  const {
    rootStore: { productStore },
  } = useStore();

  const { id } = useParams();
  const product = find(productStore.getProducts, (p) => p.id.toString() === id);
  const onClickBuyNow = () => {};
  return (
    <div className="row featurette">
      <div className="col-md-7 order-md-2">
        <h2 className="featurette-heading">{product?.name}</h2>
        <p className="lead">{product?.description}</p>
        <div>
          <button
            type="button"
            onClick={onClickBuyNow}
            className="btn btn-primary"
          >
            BUY NOW
          </button>
        </div>
      </div>
      <div className="col-md-5 order-md-1">
        <img
            width="100%"
            height="400"
            src={product.image[0] ? product.image[0] : process.env.REACT_APP_NOT_AVAIL_IMAGE_URL}
            onError={({ currentTarget }) => {
              currentTarget.onerror = null; // prevents looping
              currentTarget.src=process.env.REACT_APP_NOT_AVAIL_IMAGE_URL;
            }}
            alt="product"
          />
      </div>
    </div>
  );
});

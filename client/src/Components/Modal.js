import { Modal, Button } from "react-bootstrap"

export const ModalComponent = (props) => {
  const { title, show, toggleModal, content, onSubmit } = props;

  return (
    <Modal
      centered
      show={show}
      onHide={toggleModal}
    >
      <Modal.Header>
        <Modal.Title>
          {title}
        </Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {content}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={toggleModal}>Close</Button>
        <Button variant="primary" onClick={onSubmit}>
          Save Changes
        </Button>
      </Modal.Footer>
    </Modal>
  )
}

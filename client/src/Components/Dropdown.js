import { observer } from "mobx-react-lite";
import { useState } from "react"
import { useStore } from "../Hooks/useStore";

export const Dropdown = observer(() => {
  const { rootStore: { productStore } } = useStore();

  const [dropdownTitle, setDropdownTitle] = useState("Sort Product")

  const handleSort = async (param) => {
    setDropdownTitle(param.title);
    productStore.setProductParams(param);
    productStore.setPageNumber(1)
    productStore.setProducts([]);
    await productStore.fetchProducts()
  }

  return (
    <div className="btn-group">
      <button className="btn btn-secondary dropdown-toggle my-1" type="button" data-bs-toggle="dropdown" aria-expanded="false">
        {dropdownTitle}
      </button>
      <ul className="dropdown-menu">
        {productStore.sortParam.map((param, idx) => {
          return <li key={idx}><button className="dropdown-item" onClick={() => handleSort(param)}>{param.title}</button></li>
        })}
      </ul>
    </div>
  )
});

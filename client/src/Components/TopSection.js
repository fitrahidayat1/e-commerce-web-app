import { observer } from "mobx-react-lite";
import { useState } from "react";
import { Form } from "react-bootstrap";
import { useStore } from "../Hooks/useStore";
import { ModalComponent } from "./Modal";
import { Dropdown } from "./Dropdown"

export const TopSection = observer(() => {

  const { rootStore: { authStore, productStore } } = useStore();
  const [showModal, setShowModal] = useState(false);
  const [name, setName] = useState()
  const [image, setImage] = useState()
  const [price, setPrice] = useState()
  const [description, setDescription] = useState()

  const toggleModal = () => setShowModal(!showModal);

  const addProduct = async () => {
    const imageArr = image.split(",")
    const newProduct = { name, image:imageArr, price, description }
    console.log(newProduct);
    await productStore.addProduct(newProduct)
    toggleModal()
  }

  const modalContent = (
    <Form>
      <Form.Group className="mb-3">
        <Form.Label>Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Product Name"
          autoFocus
          onChange={e => setName(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Image URL</Form.Label>
        <Form.Control
          type="text"
          placeholder="Product Image URL"
          onChange={e => setImage(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="text"
          placeholder="Product Price"
          onChange={e => setPrice(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Description</Form.Label>
        <Form.Control
          as="textarea"
          rows={3}
          onChange={e => setDescription(e.target.value)} />
      </Form.Group>
    </Form>
  )

  return (
    <section className="py-5 text-center container">
      <div className="row py-lg-5">
        <div className="col-lg-6 col-md-8 mx-auto">
          <h1 className="fw-light">Product Catalog</h1>
          <p className="lead text-muted">Something short and leading about the collection below—its contents, the creator, etc. Make it short and sweet, but not too short so folks don’t simply skip over it entirely.</p>
          <div>
              <a href="/" className="btn btn-secondary my-1 me-3">Filter By Category</a>
              {authStore.getUserDetails ?
                <>
                <button className="btn btn-primary my-1 me-3" onClick={toggleModal}>Add Product</button>
                <ModalComponent
                    title="Add New Product"
                    show={showModal}
                    content={modalContent}
                    onSubmit = {addProduct}
                    toggleModal={toggleModal} />
                </> : ''
              }
              <Dropdown />
          </div>
        </div>
      </div>
    </section>
  );
});

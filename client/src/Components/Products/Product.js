import { forwardRef, useEffect, useState } from "react";
// import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";
import { Button, Form } from "react-bootstrap"
import { ModalComponent } from "../Modal";
import { useStore } from "../../Hooks/useStore";

export const Product = forwardRef((props, ref) => {
  const { data, user, handleDelete } = props;
  const { rootStore: { productStore } } = useStore();

  const formatter = new Intl.NumberFormat('IDR', {
  style: 'decimal',
  currency: 'IDR',
});

  const [dataProduct, setDataProduct] = useState(data);
  const [showUpdateModal, setShowUpdateModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [name, setName] = useState(data.name)
  const [image, setImage] = useState(data.image)
  const [price, setPrice] = useState(data.price)
  const [description, setDescription] = useState(data.description)
 
  const toggleUpdateModal = () => setShowUpdateModal(!showUpdateModal);
  const toggleDeleteModal = () => setShowDeleteModal(!showDeleteModal);

  const updateProduct = async () => {
    const updateData = { name, image, price, description }
    const newData = await productStore.updateProduct(updateData)
    setDataProduct(newData)
    toggleUpdateModal()
  }

  useEffect(() => {
    if (showUpdateModal) {
      productStore.currentProduct = data
    };
  })

  const modalContent = (
    <Form>
      <Form.Group className="mb-3">
        <Form.Label>Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Product Name"
          autoFocus
          value={name}
          onChange={e => setName(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Image URL</Form.Label>
        <Form.Control
          type="text"
          placeholder="Product Image URL"
          value={image}
          onChange={e => setImage(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Price</Form.Label>
        <Form.Control
          type="text"
          placeholder="Product Price"
          value={price}
          onChange={e => setPrice(e.target.value)}
        />
      </Form.Group>
      <Form.Group className="mb-3">
        <Form.Label>Description</Form.Label>
        <Form.Control
          as="textarea"
          rows={3}
          value={description}
          onChange={e => setDescription(e.target.value)} />
      </Form.Group>
    </Form>
  )

  return (
    <div className="col" ref={ref}>
      <div className="card shadow-sm">
        <Link to={`/product/${dataProduct.id}`}>
          <img
            width="100%"
            height="300"
            src={dataProduct.image[0] ? dataProduct.image[0] : process.env.REACT_APP_NOT_AVAIL_IMAGE_URL}
            onError={({ currentTarget }) => {
              currentTarget.onerror = null;
              currentTarget.src=process.env.REACT_APP_NOT_AVAIL_IMAGE_URL;
            }}
            alt="product"
          />
        </Link>
        <div className="card-body">
          <p className="card-text">{dataProduct.name}</p>
          <div className="d-flex justify-content-between align-items-center">
            <div className="btn-group">
              {user ? 
                <>
                  <Button className="me-2" variant="secondary" onClick={toggleUpdateModal} >
                    Edit
                  </Button>
                  <Button variant="danger" onClick={toggleDeleteModal}>
                    Delete
                  </Button>
                  <ModalComponent
                    title="Edit Product"
                    show={showUpdateModal}
                    content={modalContent}
                    onSubmit = {updateProduct}
                    toggleModal={toggleUpdateModal}
                  />
                  <ModalComponent
                    title="Are you sure to delete this product ?"
                    show={showDeleteModal}
                    content={"This proccess cannot be undone."}
                    onSubmit = {handleDelete}
                    toggleModal={toggleDeleteModal}
                  />
                </> : null
              }
                  </div>
            <b>{`Rp. ${formatter.format(dataProduct.price)}`}</b>
          </div>
        </div>
      </div>
    </div>
  );
});

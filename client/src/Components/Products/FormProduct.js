import { Button, Modal } from "react-bootstrap"

export const FormProduct = (props) => {

  const { title, content, show, handleShowClose} = props

  return (
    <>
      <Modal show={show} onHide={handleShowClose}>
        <Modal.Header closeButton>
          <Modal.Title>{ title }</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          { content }
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleShowClose}>
            Close
          </Button>
          <Button variant="primary" onClick={handleShowClose}>
            Save Changes
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  )
};

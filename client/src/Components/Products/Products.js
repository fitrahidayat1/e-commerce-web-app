import { useEffect, useRef, useCallback} from "react";
import { Product } from "./Product";
import { useStore } from "../../Hooks/useStore";
import { observer } from "mobx-react-lite";

export const Products = observer(() => {
  const { rootStore: { productStore, authStore } } = useStore();

  const currentUser = authStore.getUserDetails
  const loading = productStore.isLoading
  const hasMore = productStore.hasMore
  
  const ref = useRef()
  const lastProductElementRef = useCallback(node => {
    if (ref.current) ref.current.disconnect()
    ref.current = new IntersectionObserver(entries => {
      if (entries[0].isIntersecting && hasMore) {
        // setPageNumber(prevPageNumber => prevPageNumber + 1)
        productStore.setPageNumber(productStore.pageNumber + 1)
        productStore.fetchProducts()
      }
    })
    if (node) ref.current.observe(node)
  }, [hasMore, productStore])

  useEffect(() => {
    productStore.setIsLoading(true)
    productStore.fetchProducts()
    productStore.setIsLoading(false)
  }, [productStore]);
  return (
    <div className="album py-5 bg-light">
      <div className="container">
        <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 g-3">
          {productStore.products.map((p, idx) => {
            if (productStore.products.length === idx + 1) {
              return <Product data={p} key={p.id} ref={lastProductElementRef} handleDelete={() => productStore.deleteProduct(p) } />
            }
            return <Product data={p} key={p.id} user={currentUser} handleDelete={() => productStore.deleteProduct(p)} />
          })}
          <div>{loading && 'Loading...'}</div>
        </div>
      </div>
    </div>
  );
});

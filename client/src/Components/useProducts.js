import { useEffect, useState} from "react";
// import { Product } from "./Product";
import { useStore } from "../Hooks/useStore";
import { observer } from "mobx-react-lite";

export const useProducts = observer((pageNumber) => {
  const {
    rootStore: { productStore },
  } = useStore();

  const [loading, setLoading] = useState(true)
  const [error, setError] = useState(false)
  const [products, setProducts] = useState([])
  const [hasMore, setHasMore] = useState(false)

  useEffect(() => {
    setLoading(true)
    setError(false)
    productStore.fetchProducts(pageNumber);
    setProducts(prevProducts => {
      return [...new Set([...prevProducts, ...productStore.getProducts])]
    })
    setHasMore(productStore.getProducts.length > 0)
    setLoading(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pageNumber]);
  return (loading, error, products, hasMore);
});

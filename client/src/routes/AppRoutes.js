import { Routes, Route } from "react-router-dom";
import { Login } from "../Pages/Login";
import { ProductView } from "../Components/ProductView";
import { MainPage } from "../Pages/MainPage";
import { Signup } from "../Pages/Signup";

export const AppRoutes = () => {
  return (
    <Routes>
      <Route path="/" element={<MainPage />} />
      <Route path="/products" element={<MainPage />} />
      <Route path="/signup" element={<Signup />} />
      <Route path="/login" element={<Login />} />
      <Route path="/logout" element={<Login />} />
      <Route path="/product/:id" element={<ProductView />} />
    </Routes>
  );
};

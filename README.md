# e-commerce web app

Web API endpoints for Product and display list of all products, and detailed products.
Technology stack:
- Frontend: React JS, MobX
- Backend: Node JS, Hapi, Postgresql

# How to run

## Server

### Environment
Rename example.env file to .env

- DB_HOST = 'localhost'
- DB_NAME = 'postgres'
- DB_USER =  'postgres'
- DB_PASSWORD =  'your password'
- DB_PORT = 5432

- JWT_SECRET_KEY = 'your_secret_key'
- OPEN_API_KEY = 'your api key'
- URL_PRODUCT = 'http://api.elevenia.co.id/rest/prodservices/product'

Migration and Fetching Data script available on server/db/migration folder

### Available Script
```bash
$ cd server
$ npm i
$ npm run db_migrate
$ npm run db_seed
$ npm start
```

### API List

| Method | EndPoint              | Description                            |
| ------ | --------------------- | ---------------------------------------|
| POST    | /api/v1/auth/login   | payload = {email: "", password: ""}    |
| POST    | /api/v1/auth/signup  | default user:                          |
|        |                       | email : babang_tamvan@gmail.com        |
|        |                       | password : babang_tamvan               |
| PUT    | /api/v1/user/{id}     | Update a user by id                    |
| DELETE | /api/v1/user/{id}     | Delete a user by id                    |
| GET    | /api/v1/products      | List of All products                   |
|        |                       | ex: /api/v1/products?limit=10&page=1   |
| GET    | /api/v1/product/{id}  | GET Single Product by id               |
| PUT    | /api/v1/product/{id}  | Update Single product by id            |
| DELETE | /api/v1/product/{id}  | Delete a product by id                 |
| POST   | /api/v1/product       | Added new product                      |

---


## Client

### Environment
- Rename example.env file to .env

### Available Script
```bash
$ cd client
$ npm i
$ npm start
```
Open http://localhost:3000 to view it in the browser.




const Joi = require("@hapi/joi");
const productHandler = require("../handlers/productHandler");

module.exports = [
  {
    method: "GET",
    path: "/products",
    config: {
      description: "GET All Products",
      validate: {
        query: Joi.object({
          limit: Joi.number().default(10).min(1).max(100),
          page: Joi.number().default(1).positive(),
          sort_by: Joi.string().default("created_at"),
          order: Joi.string().default("desc"),
        }),
      },
    },
    handler: productHandler.getAllProductsHandler,
  },
  {
    method: "GET",
    path: "/product/{id}",
    handler: productHandler.getProductHandler,
  },
  {
    method: "PUT",
    path: "/product/{id}",
    config: {
      description: "Update Product by id",
      auth: "jwt_strategy",
    },
    handler: productHandler.updateProductHandler,
  },
  {
    method: "POST",
    path: "/product",
    config: {
      description: "Added New Product",
      validate: {
        payload: Joi.object({
          name: Joi.string().required(),
          image: Joi.array(),
          price: Joi.number(),
          description: Joi.string(),
        }),
      },
      auth: "jwt_strategy",
    },
    handler: productHandler.createProductHandler,
  },
  {
    method: "DELETE",
    path: "/product/{id}",
    config: {
      description: "Delete Product by id",
      validate: {
        params: Joi.object({
          id: Joi.number(),
        }),
      },
      auth: "jwt_strategy",
    },
    handler: productHandler.deleteProductHandler,
  },
  {
    method: "GET",
    path: "/product/:sort_by:order",
    config: {
      description: "Sort Product",
      validate: {
        query: Joi.object({
          sort_by: Joi.string(),
          order: Joi.string(),
        }),
      },
    },
    handler: productHandler.sortProductHandler,
  },
];

/* eslint-disable import/no-dynamic-require */
const fs = require("fs");

let routes = [];

// eslint-disable-line global-require
// eslint-disable-line import/no-dynamic-require
fs.readdirSync(__dirname)
  .filter((file) => file !== "index.js")
  .forEach((file) => {
    // eslint-disable-next-line import/no-dynamic-require
    // eslint-disable-next-line global-require
    routes = routes.concat(require(`./${file}`));
  });

module.exports = routes;

const authHandler = require("../handlers/authHandler");

module.exports = [
  {
    method: "POST",
    path: "/auth/login",
    config: {
      description: "Login User",
    },
    handler: authHandler.login,
  },
  {
    method: "POST",
    path: "/auth/signup",
    config: {
      description: "Signup User",
    },
    handler: authHandler.signup,
  },
];

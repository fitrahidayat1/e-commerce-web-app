const Joi = require("@hapi/joi");
const userHandler = require("../handlers/userHandler");

module.exports = [
  {
    method: "GET",
    path: "/users",
    config: {
      description: "GET All Users",
      validate: {
        query: Joi.object({
          limit: Joi.number().default(10).min(1).max(100),
          page: Joi.number().default(1).positive(),
        }),
      },
      auth: "jwt_strategy",
    },
    handler: userHandler.getAllUsers,
  },
  {
    method: "GET",
    path: "/user",
    config: {
      description: "GET User by id",
      auth: "jwt_strategy",
    },
    handler: userHandler.getUser,
  },
  // {
  //   method: "PUT",
  //   path: "/user/{id}",
  //   handler: productHandler.updateProductHandler,
  // },
  {
    method: "DELETE",
    path: "/user/{id}",
    config: {
      description: "Delete a user by id",
      auth: "jwt_strategy",
    },
    handler: userHandler.deleteUser,
  },
];

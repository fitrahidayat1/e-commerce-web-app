const commonQuery = require("../../db/queryHandler");
const productQuery = require("../../db/queryHandler/productQuery");

exports.getAllProductsHandler = async (req, reply) => {
  const params = { ...req.query };
  params.offset = (params.page - 1) * params.limit;
  const data = await commonQuery.findAll(
    "products",
    params.limit,
    params.offset,
    params.sort_by,
    params.order,
  );
  const response = reply.response({
    status: "success",
    data: { total: data.rowCount, row: data.rows },
  });
  response.code(200);
  return response;
};

exports.getProductHandler = async (req, reply) => {
  const { id } = req.params;
  const product = await commonQuery.findOne("products", { id });
  const response = reply.response({
    status: "success",
    product,
  });
  response.code(200);
  return response;
};

exports.createProductHandler = async (req, reply) => {
  const { payload } = req;
  const newSku = await productQuery.getNextSku();
  payload.sku = newSku;
  const result = await commonQuery.insertOne("products", payload);
  const response = reply.response({
    status: "success",
    result,
  });
  response.code(200);
  return response;
};

exports.updateProductHandler = async (req, reply) => {
  const { id } = req.params;
  const updateData = req.payload;
  await commonQuery.updateOne("products", updateData, { id });
  const newData = await commonQuery.findOne("products", { id });
  const response = reply.response({
    status: "success",
    product: newData,
  });
  response.code(200);
  return response;
};

exports.deleteProductHandler = async (req, replay) => {
  const { id } = req.params;
  const result = await commonQuery.removeOne("products", { id });
  const response = replay.response({
    status: "success",
    result: { total: result.rowCount, row: result.rows },
  });
  response.code(200);
  return response;
};

exports.sortProductHandler = async (req, reply) => {
  const { sortBy, order } = req.query;
  const result = await productQuery.sortProduct(sortBy, order);
  const response = reply.response({
    status: "success",
    result: { total: result.rowCount, row: result.rows },
  });
  response.code(200);
  return response;
};

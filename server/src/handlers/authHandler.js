const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const boom = require("@hapi/boom");
const query = require("../../db/queryHandler");
require("dotenv").config();

exports.login = async (req, reply) => {
  const { email, password } = req.payload;
  const validUser = await query.findOne("users", { email });
  if (validUser && (bcrypt.compareSync(password, validUser.password))) {
    const token = jwt.sign({
      user_id: validUser.id,
      aud: "urn:audience:test",
      iss: "urn:issuer:test",
      sub: false,
      maxAgeSec: 14400,
      timeSkewSec: 15,
    }, process.env.JWT_SECRET_KEY);
    const response = reply.response({
      status: "success",
      token,
    });
    response.code(200);
    return response;
  }
  return boom.unauthorized("incorrect pass");
};

exports.signup = async (req, reply) => {
  const salt = bcrypt.genSaltSync(10);
  req.payload.password = bcrypt.hashSync(req.payload.password, salt);
  query.insertOne("users", req.payload).catch((err) => {
    if (err) {
      const response = reply.response({
        status: "fail",
      });
      response.code(500);
    }
  });
  const response = reply.response({
    status: "success",
  });
  response.code(200);
  return response;
};

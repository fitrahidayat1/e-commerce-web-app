const queryHandler = require("../../db/queryHandler");

exports.getAllUsers = async (req, reply) => {
  const params = { ...req.query };
  params.offset = (params.page - 1) * params.limit;
  const data = await queryHandler.findAll(
    "users",
    params.limit,
    params.offset,
  );
  const response = reply.response({
    status: "success",
    total: data.rowCount,
    users: data.rows,
  });
  response.code(200);
  return response;
};

exports.getUser = async (req, reply) => {
  const id = req.auth.artifacts.decoded.payload.user_id;
  const user = await queryHandler.findOne("users", { id });
  const response = reply.response({
    status: "success",
    user: {
      firstName: user.first_name,
      lastName: user.last_name,
      email: user.email,
    },
  });
  response.code(200);
  return response;
};

exports.deleteUser = async (req, reply) => {
  const { id } = req.params;
  const result = await queryHandler.removeOne("users", { id });
  const response = reply.response({
    status: "success",
    result: { total: result.rowCount, row: result.rows },
  });
  response.code(200);
  return response;
};

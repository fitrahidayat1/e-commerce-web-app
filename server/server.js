/* eslint-disable no-console */
const Hapi = require("@hapi/hapi");
const Jwt = require("@hapi/jwt");
// const hapiAuthJWT = require("./lib");
// const auth = require("./src/middleware/auth");
const routes = require("./src/routes");
require("dotenv").config();

const init = async () => {
  // Config Server
  const server = new Hapi.Server({
    port: 5000,
    host: "localhost",
    routes: { cors: { origin: ["*"] } },
  });
  await server.register(Jwt);
  server.auth.strategy("jwt_strategy", "jwt", {
    keys: process.env.JWT_SECRET_KEY,
    verify: {
      aud: "urn:audience:test",
      iss: "urn:issuer:test",
      sub: false,
      nbf: true,
      exp: true,
      maxAgeSec: 14400,
      timeSkewSec: 15,
    },
    validate: (artifacts) => ({
      isValid: true,
      credentials: { user: artifacts.decoded.payload.id },
    }),
  });

  // Prefix for any routes
  server.realm.modifiers.route.prefix = "/api/v1";

  // Home Routes
  server.route([
    {
      method: "GET",
      path: "/",
      handler: () => "Welcome to e-commerce web app API",
    },
    {
      method: "*",
      path: "/{any*}",
      handler: () => "You've tried reaching a route that doesn't exist.",
    },
  ]);

  // Modular Routes
  server.route(routes);

  await server.start();
  return server;
};

init().then((server) => {
  console.log(`Server running on ${server.info.uri}`);
})
  .catch((err) => {
    console.log(err);
  });

module.exports = init;

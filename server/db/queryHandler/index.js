const { query } = require("../config");

exports.findAll = async (tableName, limit, offset, sortBy, order) => {
  if (!tableName) throw new Error("No entity table specified");
  const sql = `SELECT * from ${tableName} ORDER BY ${sortBy} ${order} limit ${limit} offset ${offset}`;
  const result = await query(sql);
  return result;
};

exports.findOne = async (tableName, conditions = {}) => {
  if (!tableName) throw new Error("No entity table specified");
  const sql = [`SELECT * FROM ${tableName} WHERE`];
  const where = [];
  Object.keys(conditions).forEach((key) => {
    where.push(`${key} = '${conditions[key]}'`);
  });
  sql.push(where.join(" AND "));
  const result = await query(sql.join(" "));
  return result?.rows[0];
};

exports.updateOne = async (tableName, updateData = {}, conditions = {}) => {
  if (!tableName) throw new Error("No entity table specified");
  const sql = [`UPDATE ${tableName}`];

  // ADD SET statement
  sql.push("SET");
  const set = [];
  Object.keys(updateData).forEach((key) => {
    const value = updateData[key];
    if (Array.isArray(value)) {
      set.push(`${key} = '{${value.join(", ")}}'`);
    } else {
      set.push(`${key} = '${value}'`);
    }
  });
  sql.push(set.join(", "));

  // ADD WHERE condition
  const where = [];
  sql.push("WHERE");
  Object.keys(conditions).forEach((key) => {
    where.push(`${key} = ${conditions[key]}`);
  });
  sql.push(where.join(" AND "));
  const result = await query(sql.join(" "));
  return result;
};

exports.insertOne = async (tableName, insertData = {}) => {
  if (!tableName) throw new Error("No entity table specified");
  const sql = [`INSERT INTO ${tableName}`];
  const columnArr = [];
  const valuesArr = [];
  Object.keys(insertData).forEach((key) => {
    columnArr.push(key);
    if (Array.isArray(insertData[key])) {
      valuesArr.push(`'{${insertData[key].join(", ")}}'`);
    } else {
      valuesArr.push(`'${insertData[key]}'`);
    }
  });
  const columnStr = `( ${columnArr.join(", ")} )`;
  const valuesStr = `values ( ${valuesArr.join(", ")} )`;
  sql.push(columnStr);
  sql.push(valuesStr);
  // const result = await query(sql.join(" "));
  // return result;
  try {
    await query(sql.join(" "));
  } catch (error) {
    if (error) throw error;
  }
};

exports.removeOne = async (tableName, conditions = {}) => {
  if (!tableName) throw new Error("No entity table specified");
  const sql = [`DELETE FROM ${tableName} WHERE`];
  const removeCondition = [];
  Object.keys(conditions).forEach((key) => {
    removeCondition.push(`${key} = '${conditions[key]}'`);
  });
  sql.push(removeCondition.join(" AND "));
  const result = await query(sql.join(" "));
  return result?.rows;
};

const { query } = require("../config");

const tableName = "products";

exports.getNextSku = async () => {
  const sql = `SELECT sku FROM ${tableName} ORDER BY sku DESC LIMIT 1`;
  const result = await query(sql);
  const lastSku = result?.rows[0].sku;
  const nextSku = parseInt(lastSku, 10) + 1;
  return nextSku.toString();
};

exports.addNewProduct = async (name, sku, image, price, description) => {
  const sql = `INSERT INTO ${tableName} (name, sku, image, price, description) VALUES($1, $2, $3, $4, $5)`;
  const sqlData = [name, sku, image, price, description];
  const result = await query(sql, sqlData);
  return result;
};

exports.sortProduct = async (sortBy, order) => {
  const sql = `SELECT * FROM ${tableName} ORDER BY ${sortBy} ${order}`;
  const result = await query(sql);
  return result;
};

/* eslint-disable no-console */
const { Pool } = require("pg");
require("dotenv").config();

let mainPool = null;
const createPool = () => {
  const pool = new Pool({
    host: process.env.DB_HOST,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    port: process.env.DB_PORT,
    connectionTimeoutMillis: 0,
    idleTimeoutMillis: 0,
    min: 10,
    max: 20,
  });
  return pool;
};

exports.getPool = () => {
  if (!mainPool) {
    mainPool = createPool();
  }
  return mainPool;
};

exports.query = async (sql, sqlData) => {
  try {
    const start = Date.now();
    const result = await this.getPool().query(sql, sqlData);
    const duration = Date.now() - start;
    console.log("Executed query", {
      sql, sqlData, duration, rowCount: result?.rowCount,
    });
    return result;
  } catch (error) {
    console.log(error.message);
    throw error.message;
  }
};

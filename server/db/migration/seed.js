/* eslint-disable no-console */
const fetch = require("node-fetch");
const { DOMParser } = require("xmldom");
const { select } = require("xpath");
const productQuery = require("../queryHandler/productQuery");
const commonQuery = require("../queryHandler/index");
require("dotenv").config();

const options = {
  method: "GET",
  headers: {
    "Content-Type": "application/xml",
    "Accept-Charset": "utf-8",
    openapikey: process.env.OPEN_API_KEY,
  },
};

async function fetchProductDetail(productNum) {
  const response = await fetch(`${process.env.URL_PRODUCT}/details/${productNum}`, options);
  const data = await response.text();
  const dataParsed = new DOMParser().parseFromString(data);

  // GET Element XML
  const productName = dataParsed.getElementsByTagName("prdNm")[0].firstChild.nodeValue;
  const productPrice = dataParsed.getElementsByTagName("selPrc")[0].firstChild.nodeValue;
  const productDesc = dataParsed.getElementsByTagName("htmlDetail")[0].firstChild.nodeValue;

  // GET Image Element on XML
  const imageArr = [];
  const queryXml = '//*[contains(local-name(), "prdImage")]';
  const nodes = select(queryXml, dataParsed);
  nodes.forEach((node) => {
    const imageUrl = node.firstChild.nodeValue;
    imageArr.push(imageUrl);
  });

  // INSERT to Database
  productQuery.addNewProduct(productName, productNum, imageArr, productPrice, productDesc);
}

let pageNumber = 1;
async function fetchProduct() {
  const response = await fetch(`${process.env.URL_PRODUCT}/listing?page=${pageNumber}`, options);
  const data = await response.text();
  const dataParsed = new DOMParser().parseFromString(data);
  const queryXml = '//*[contains(local-name(), "prdNo")]';
  const nodes = select(queryXml, dataParsed);
  if (nodes.length > 0) {
    for (let i = 0; i < nodes.length; i += 1) {
      const productNum = nodes[i].firstChild.nodeValue;
      // eslint-disable-next-line no-await-in-loop
      const sku = await commonQuery.findOne("products", { sku: productNum });
      if (!sku) fetchProductDetail(productNum);
    }
    pageNumber += 1;
    fetchProduct();
  } else {
    console.log("Fetch product done!");
  }
}

fetchProduct();

/* eslint-disable no-console */
const fs = require('fs');
const { getPool } = require('../config');
require('dotenv').config();

if (process.env.NODE_ENV !== 'production') {
  const migrateQuery = fs.readFileSync('db/migration/migrate.sql', { encoding: 'utf8' });
  getPool().query(migrateQuery, (err) => {
    if (err) throw err;
    console.log('Migrate Completed!');
    getPool().end();
  });
}
